package keycloak

import (
	"context"
	"errors"
	"fmt"
	"github.com/Nerzal/gocloak/v13"
)

type Client struct {
	client *gocloak.GoCloak
	ctx    context.Context
	token  *gocloak.JWT
}

func LoginKeycloakClient(url string, username string, password string, realmName string) (*Client, error) {
	client := gocloak.NewClient(url)
	ctx := context.Background()
	token, err := client.LoginAdmin(ctx, username, password, realmName)
	if err != nil {
		return nil, err
	}
	return &Client{
		client,
		ctx,
		token,
	}, nil
}

func (k *Client) GetUsersGroups(username string, realmName string) ([]string, error) {
	users, err := k.client.GetUsers(k.ctx, k.token.AccessToken, realmName, gocloak.GetUsersParams{Username: &username})
	if err != nil {
		return nil, err
	}
	if len(users) == 0 {
		return nil, errors.New(fmt.Sprintf("user %s not found", username))
	}
	groups, err := k.client.GetUserGroups(k.ctx, k.token.AccessToken, realmName, *users[0].ID, gocloak.GetGroupsParams{})
	if err != nil {
		return nil, err
	}
	result := make([]string, len(groups))
	for i := range groups {
		result[i] = *groups[i].Name
	}
	return result, nil
}
