package mattermost

import (
	"fmt"
	"github.com/mattermost/mattermost-server/v6/model"
	"os"
	"os/signal"
	"strings"
)

type MattermostClient struct {
	client          *model.Client4
	domain          string
	webSocketClient *model.WebSocketClient
	user            *model.User
	debugChannel    *model.Channel
}

func NewMattermostClient(domain string) MattermostClient {
	client := model.NewAPIv4Client("https://" + domain)
	return MattermostClient{
		client,
		domain,
		nil,
		nil,
		nil,
	}
}

func (m *MattermostClient) GetMyUserId() string {
	return m.user.Id
}

func (m *MattermostClient) IsLoggedIn() bool {
	return m.user != nil
}

func (m *MattermostClient) SetToken(token string) error {
	m.client.SetToken(token)

	me, _, err := m.client.GetMe("")
	if err != nil {
		return err
	}
	m.user = me
	return nil
}

func (m *MattermostClient) IsAdmin() bool {
	return strings.Contains(m.user.Roles, "admin")
}

func (m *MattermostClient) MakeUserAdmin(user *model.User) error {
	if user.Roles != "system_user system_admin" {
		_, err := m.client.UpdateUserRoles(user.Id, "system_user system_admin")
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *MattermostClient) RemoveUserAdmin(user *model.User) error {
	if user.Roles != "system_user" {
		_, err := m.client.UpdateUserRoles(user.Id, "system_user")
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *MattermostClient) CreateDebugChannel(name string, displayName string, purpose string, team *model.Team) error {
	channel, err := m.CreateChannelIfNeeded(name, displayName, purpose, team)
	if err != nil {
		return err
	}
	m.debugChannel = channel
	return nil
}

func (m *MattermostClient) SendDebugChannelMsg(msg string) error {
	return m.SendMsg(m.debugChannel, msg, nil, nil)
}

func (m *MattermostClient) SendDebugChannelAndStdoutMsg(msg string) error {
	fmt.Println(msg)
	return m.SendMsg(m.debugChannel, msg, nil, nil)
}

func (m *MattermostClient) ReportError(msg string) {
	fmt.Printf("An error occured: %s\n", msg)
	m.SendDebugChannelMsg(fmt.Sprintf("An error occured:\n\n%s", msg))
}

func (m *MattermostClient) CreateTeamIfItDoesNotExist(name string) (*model.Team, error) {
	team, err := m.GetTeam(name)
	if err != nil {
		fmt.Printf("Error on getting team: %s. Assuming team does not exist.", err)
		fmt.Printf("Cannot find main team %s, creating it\n", name)
		team, _, err := m.client.CreateTeam(&model.Team{
			Name:        name,
			DisplayName: name,
			Type:        "O",
		})
		if err != nil {
			return nil, err
		}
		return team, nil
	}
	return team, err
}

func (m *MattermostClient) GetTeam(name string) (*model.Team, error) {

	if team, _, err := m.client.GetTeamByName(name, ""); err != nil {
		return nil, err
	} else {
		return team, nil
	}
}

func (m *MattermostClient) AddUserToTeam(userId string, teamId string) error {
	_, _, err := m.client.AddTeamMember(teamId, userId)
	if err != nil {
		return err
	}
	return nil
}

func (m *MattermostClient) CreateChannelIfNeeded(
	name string, displayName string, purpose string, team *model.Team) (*model.Channel, error) {
	if channel, _, err := m.client.GetChannelByName(name, team.Id, ""); err != nil {
		// Do nothing, maybe we can create the channel
	} else {
		return channel, nil
	}

	// Looks like we need to create the channel
	channel := &model.Channel{}
	channel.Name = name
	channel.DisplayName = displayName
	channel.Purpose = purpose
	channel.Type = model.ChannelTypeOpen
	channel.TeamId = team.Id
	if channel, _, err := m.client.CreateChannel(channel); err != nil {
		return nil, err
	} else {
		return channel, nil
	}
}

func (m *MattermostClient) GetDirectMessagesChannel(otherUser string) (*model.Channel, error) {
	channel, _, err := m.client.CreateDirectChannel(m.user.Id, otherUser)
	if err != nil {
		return nil, err
	}
	return channel, nil
}

func (m *MattermostClient) SendMsg(channel *model.Channel, msg string, replyTo *model.Post, fileIds model.StringArray) error {
	post := &model.Post{}
	post.ChannelId = channel.Id
	post.Message = msg
	if fileIds != nil {
		post.FileIds = fileIds
	}

	if replyTo != nil {
		post.RootId = replyTo.RootId
		if post.RootId == "" {
			post.RootId = replyTo.Id
		}
	}

	if _, _, err := m.client.CreatePost(post); err != nil {
		return err
	}
	return nil
}

func (m *MattermostClient) StartEventListening() (chan *model.WebSocketEvent, error) {
	var err error
	m.webSocketClient, err = model.NewWebSocketClient4("wss://"+m.domain, m.client.AuthToken)
	if err != nil {
		fmt.Printf("%v\n", err)
		return nil, err
	}
	m.webSocketClient.Listen()
	return m.webSocketClient.EventChannel, nil
}

func (m *MattermostClient) GetUser(id string) (*model.User, error) {
	user, _, err := m.client.GetUser(id, "")
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (m *MattermostClient) SetupGracefulShutdown() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		for _ = range c {
			//SendMsg(debugChannel, "_"+config.Mattermost.BotName+" has **stopped** running_", nil, nil)
			if m.webSocketClient != nil {
				m.webSocketClient.Close()
			}
			os.Exit(0)
		}
	}()
}

func (m *MattermostClient) IsServerRunning() bool {
	if props, _, err := m.client.GetOldClientConfig(""); err != nil {
		fmt.Printf("There was a problem pinging the Mattermost server.  Are you sure it's running? Error: %s\n", err.Error())
		return false
	} else {
		println("Server detected and is running version " + props["Version"])
		return true
	}
}
