module mattermost_user_welcome_bot

go 1.15

require (
	github.com/Nerzal/gocloak/v13 v13.1.0
	github.com/mattermost/mattermost-server/v6 v6.7.2
)
