package main

import (
	"encoding/json"
	"fmt"
	"github.com/mattermost/mattermost-server/v6/model"
	"log"
	"mattermost_user_welcome_bot/config"
	"mattermost_user_welcome_bot/keycloak"
	"mattermost_user_welcome_bot/mattermost"
	"os"
	"strings"
)

var appConfig config.Config

func main() {
	// Load the config
	configFileName, found := os.LookupEnv("CONFIG_FILE")
	if !found {
		configFileName = "./config.json"
	}
	c, err := config.LoadConfig(configFileName)
	appConfig = c
	if err != nil {
		log.Panicf("error loading config: %s", err.Error())
	}

	client := mattermost.NewMattermostClient(appConfig.MattermostDomain)
	//client.SetupGracefulShutdown()
	if !client.IsServerRunning() {
		panic("Server is not running, stopping")
	}
	err = client.SetToken(appConfig.MattermostAccessToken)
	if err != nil {
		panic(err)
	}
	// Are we admin?
	//if !client.IsAdmin() {
	//	panic("Sorry, but I have to be admin in mattermost to work")
	//}

	// Find the team
	team, err := client.CreateTeamIfItDoesNotExist(appConfig.MainTeam)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Main team (%s) has the id %s\n", team.Name, team.Id)
	// Create debug channel
	err = client.CreateDebugChannel("bot-debug-channel",
		"Debug Channel für Bots",
		"Hier sagen die bots was so geht",
		team)
	if err != nil {
		panic(err)
	}
	// Say hello
	err = client.SendDebugChannelMsg("_" + "welcome bot" + " has **started** running_")
	defer func() {
		client.SendDebugChannelMsg("_" + "welcome bot" + " has **stopped** running_")
	}()
	if err != nil {
		panic(err)
	}
	socketChanel, err := client.StartEventListening()
	if err != nil {
		panic(err.Error())
	}
	fmt.Printf("Started up, waiting for events\n\n")
	for {
		select {
		case event := <-socketChanel:
			HandleEvent(client, event)
		}
	}
}

func HandleEvent(client mattermost.MattermostClient, event *model.WebSocketEvent) {
	if event.EventType() == model.WebsocketEventNewUser {
		handleNewUser(client, event)
	}
	if event.EventType() == model.WebsocketEventPosted {
		var post = model.Post{}
		json.NewDecoder(strings.NewReader(event.GetData()["post"].(string))).Decode(&post)
		// ignore my events
		if post.UserId == client.GetMyUserId() {
			return
		}
		// Get our direct messaging channel
		directChannel, err := client.GetDirectMessagesChannel(post.UserId)
		if err != nil {
			client.ReportError(fmt.Sprintf("unable to get direct channel to user: %v", err))
		}
		// Is it a direct message?
		if directChannel.Id == post.ChannelId {
			handleDirectMessage(client, &post)
		}
	}
}

func handleDirectMessage(client mattermost.MattermostClient, post *model.Post) {
	// Get our direct messaging channel
	directChannel, err := client.GetDirectMessagesChannel(post.UserId)
	if err != nil {
		client.ReportError(fmt.Sprintf("unable to get direct channel to user: %v", err))
	}
	client.SendMsg(directChannel, appConfig.WillCheckYouPrivilegesMessage, nil, nil)
	setUsersTeamsAndPrivileges(client, post.UserId)
}

func handleNewUser(client mattermost.MattermostClient, event *model.WebSocketEvent) {
	// Get the user
	newUserId := event.GetData()["user_id"].(string)
	setUsersTeamsAndPrivileges(client, newUserId)
}

func setUsersTeamsAndPrivileges(client mattermost.MattermostClient, userId string) {
	client.SendDebugChannelAndStdoutMsg(fmt.Sprintf("New user with id %s\n", userId))
	// Getting user infos
	user, err := client.GetUser(userId)
	if err != nil {
		client.ReportError(fmt.Sprintf("unable to get the user from mattermost: %v", err))
		return
	}
	// Start chatting with him
	directChannel, err := client.GetDirectMessagesChannel(userId)
	if err != nil {
		client.ReportError(fmt.Sprintf("unable to get a message channel to the user: %v", err))
		return
	}
	// Tell the user hello
	err = client.SendMsg(directChannel,
		strings.ReplaceAll(appConfig.WelcomeMessage, "{username}", user.Username),
		nil,
		nil)
	client.SendDebugChannelAndStdoutMsg(fmt.Sprintf("I said hello to %s\n", userId))
	if err != nil {
		client.ReportError(fmt.Sprintf("unable to send message to user: %v", err))
		client.SendMsg(directChannel, appConfig.ErrorMessage, nil, nil)
		return
	}
	// Debug output the result
	client.SendDebugChannelAndStdoutMsg(fmt.Sprintf("I know now, that %s is %s\n", userId, user.Username))

	kc, err := keycloak.LoginKeycloakClient(appConfig.KeycloakUrl, appConfig.KeycloakUsername, appConfig.KeycloakPassword, "master")
	if err != nil {
		client.ReportError(fmt.Sprintf("unable to login to keycloak: %v", err))
		client.SendMsg(directChannel, appConfig.ErrorMessage, nil, nil)
		return
	}
	groups, err := kc.GetUsersGroups(user.Username, appConfig.KeycloakRealm)
	if err != nil {
		client.ReportError(fmt.Sprintf("unable to get groups from keycloak: %v", err))
		client.SendMsg(directChannel, appConfig.ErrorMessage, nil, nil)
		return
	}
	client.SendDebugChannelAndStdoutMsg(fmt.Sprintf("In keycloak %s has these groups: %v", user.Username, groups))

	//Find out if he is admin
	var isAdmin bool = false
	for _, group := range groups {
		if group == appConfig.AdminGroup {
			fmt.Printf("%s is admin!\n", user.Username)
			isAdmin = true
			client.SendMsg(directChannel, appConfig.MakeAdminMessage, nil, nil)
			err = client.MakeUserAdmin(user)
			if err != nil {
				client.ReportError(fmt.Sprintf("unable to make the user an admin: %v", err))
				client.SendMsg(directChannel, appConfig.ErrorMessage, nil, nil)
			}
		}
	}
	if !isAdmin {
		err = client.RemoveUserAdmin(user)
		if err != nil {
			client.ReportError(fmt.Sprintf("unable to remive admin piviliges from user: %v", err))
			client.SendMsg(directChannel, appConfig.ErrorMessage, nil, nil)
		}
	}

	// Find the relevant teams
	teams := []string{}
	for _, group := range groups {
		if team, ok := appConfig.GroupTeamMapping[group]; ok {
			teams = append(teams, team)
		}
	}
	if len(teams) == 0 {
		// Tell the user what is happening
		err = client.SendMsg(directChannel,
			appConfig.NoTeamsToAddMessage,
			nil,
			nil)
		return
	}
	// Tell the user the groups
	err = client.SendMsg(directChannel,
		strings.ReplaceAll(appConfig.AddToTeamsMessage, "{teams}", strings.Join(teams, ", ")),
		nil,
		nil)
	if err != nil {
		client.ReportError(fmt.Sprintf("unable to send message to user: %v", err))
		client.SendMsg(directChannel, appConfig.ErrorMessage, nil, nil)
		return
	}
	client.SendDebugChannelAndStdoutMsg(fmt.Sprintf("I found these teams for %s: %v", user.Username, teams))
	// Add user to teams
	for _, team := range teams {
		teamInst, err := client.GetTeam(team)
		if err != nil {
			client.ReportError(fmt.Sprintf("error searching for team %s for user %s: %s", team, user.Username, err.Error()))
			client.SendMsg(directChannel, fmt.Sprintf("There was a problem getting the team %s, so I cannot add you :(", teams), nil, nil)
			client.SendMsg(directChannel,
				strings.ReplaceAll(appConfig.CannotAddToTeamMessage, "{team}", team),
				nil,
				nil)
			continue
		}
		err = client.AddUserToTeam(user.Id, teamInst.Id)
		if err != nil {
			client.ReportError(fmt.Sprintf("error adding user %s to team %s: %s", user.Username, team, err.Error()))
			client.SendMsg(directChannel,
				strings.ReplaceAll(appConfig.CannotAddToTeamMessage, "{team}", team),
				nil,
				nil)
			return
		}
	}
	client.SendDebugChannelAndStdoutMsg(fmt.Sprintf("Done with %s (%s)", user.Username, userId))
}
