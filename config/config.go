package config

import (
	"encoding/json"
	"io/ioutil"
)

type Config struct {
	MattermostAccessToken string `json:"mm_access_token"`
	MattermostDomain string `json:"mm_domain"`
	KeycloakUsername string `json:"kc_username"`
	KeycloakPassword string `json:"kc_password"`
	KeycloakUrl string `json:"kc_url"`
	KeycloakRealm string `json:"kc_realm"`
	MainTeam string `json:"main_team"`
	GroupTeamMapping map[string]string `json:"group_team_mapping"`
	AdminGroup string `json:"admin_group"`
	WelcomeMessage string `json:"welcome_message"`
	AddToTeamsMessage string `json:"add_to_teams_message"`
	NoTeamsToAddMessage string `json:"no_teams_to_add_message"`
	MakeAdminMessage string `json:"make_admin_message"`
	ErrorMessage string `json:"error_message"`
	CannotAddToTeamMessage string `json:"cannot_add_to_team_message"`
	WillCheckYouPrivilegesMessage string `json:"will_check_your_privileges_message"`
}

func LoadConfig(filename string) (Config, error) {
	var config = Config{}
	bytes, err := ioutil.ReadFile(filename)
	if err != nil {
		return config, err
	}
	err = json.Unmarshal(bytes, &config)
	if err != nil {
		return config, err
	}
	return config, nil
}